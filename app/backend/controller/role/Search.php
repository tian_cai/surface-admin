<?php

namespace app\backend\controller\role;

use app\backend\model\Role as RoleModel;
use surface\form\components\Input;
use surface\helper\FormAbstract;

class Search extends FormAbstract
{

    public function rules(): array
    {
        return [
            'title' => 'LIKE'
        ];
    }

    public function columns(): array
    {
        return [
            new Input('title', RoleModel::$labels['title'])
        ];
    }

}
