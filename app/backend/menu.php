<?php

/**
 * 菜单和权限配置
 *
 * 菜单和权限非日常操作功能 改用文件保存
 *
 * 如果需要数据库保存 请自行修改
 *
 * 配置格式
 * [
 *      'name'      => string       '名称',
 *      'menu'      => bool         'true菜单 | false权限',
 *      'filter'    => string       '认证方式：*所有人访问 @登录访问 &授权访问【默认】',
 *      'route'     => string       '路由',
 *      'method'    => string|array '请求方式：GET POST PUT OPTION POST *【默认】',
 *      'icon'      => string       '菜单图标' remixicon图标  http://remixicon.cn/
 *      'children'  => array        '子节点'
 * ]
 *
 */

$prefix = '/backend';

$curdPrefix = config('curd.route_prefix');

$composerFile = root_path() . 'composer.lock';
$curdVersion = '';
if (is_file($composerFile)) {
    $composer = file_get_contents($composerFile);
    $composer = json_decode($composer, true);
    foreach ($composer['packages'] as $p) {
        if ($p['name'] === 'iszsw/curd') {
            $curdVersion = $p['version'];
            break;
        }
    }
}

return [
    [
        'name'   => '登录',
        'menu'   => false,
        'filter' => '*',
        'route'  => $prefix.'/login',
        'method' => ['GET', 'POST'],
    ],
    [
        'name'   => '登出',
        'menu'   => false,
        'filter' => '@',
        'route'  => $prefix.'/index/logout',
        'method' => 'GET',
    ],
    [
        'name'   => '首页',
        'menu'   => false,
        'filter' => '@',
        'route'  => $prefix.'/',
        'method' => 'GET',
    ],
    [
        'name'   => '清理缓存',
        'menu'   => false,
        'filter' => '@',
        'route'  => $prefix.'/index/clean',
        'method' => 'GET',
    ],
    [
        'name'     => '控制台',
        'filter'   => '@',
        'icon'     => 'ri-home-4-line',
        'menu'     => true,
        'route'    => '#home',
        'children' => [
            [
                'filter' => '@',
                'name'   => '控制台',
                'route'  => $prefix.'/index/home',
                'icon'   => 'ri-airplay-line',
                'menu'   => true,
            ],
            [
                'filter' => '*',
                'name'   => '系统介绍',
                'route'  => $prefix.'/index/info',
                'icon'   => 'ri-file-settings-line',
                'menu'   => true,
            ],
        ],
    ],
    [
        'name'     => '系统设置',
        'icon'     => 'ri-settings-line',
        'menu'     => true,
        'route'    => '#setting',
        'children' => [
            [
                'name'     => '管理员设置',
                'icon'     => 'ri-user-4-line',
                'menu'     => true,
                'route'    => '#admin-setting',
                'children' => [
                    [
                        'name'     => '角色管理',
                        'route'    => $prefix.'/role/index',
                        'menu'     => true,
                        'children' => [
                            [
                                'name'   => '创建',
                                'method' => '*',
                                'route'  => $prefix.'/role/create',
                            ],
                            [
                                'name'   => '更新',
                                'route'  => $prefix.'/role/update',
                                'method' => '*',
                            ],
                            [
                                'name'   => '修改',
                                'route'  => $prefix.'/role/change',
                                'method' => 'POST',
                            ],
                            [
                                'name'   => '删除',
                                'route'  => $prefix.'/role/delete',
                                'method' => 'DELETE',
                            ],
                            [
                                'name'   => '分配权限',
                                'route'  => $prefix.'/role/assign',
                                'method' => '*',
                            ],
                        ],
                    ],

                    [
                        'name'     => '管理员',
                        'route'    => $prefix.'/admin/index',
                        'menu'     => true,
                        'method'   => 'GET',
                        'children' => [
                            [
                                'name'   => '新增',
                                'method' => '*',
                                'route'  => $prefix.'/admin/create',
                            ],
                            [
                                'name'   => '更新',
                                'route'  => $prefix.'/admin/update',
                                'method' => '*',
                            ],
                            [
                                'name'   => '修改',
                                'route'  => $prefix.'/admin/change',
                                'method' => 'POST',
                            ],
                            [
                                'name'   => '删除',
                                'route'  => $prefix.'/admin/delete',
                                'method' => 'DELETE',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name'     => '附件管理',
                'route'    => $prefix.'/attachment/index',
                'icon'     => 'ri-image-line',
                'menu'     => true,
                'filter'   => '@',
                'children' => [
                    [
                        'name'   => '管理',
                        'route'  => $prefix.'/attachment/manage',
                        'method' => 'GET',
                        'filter' => '@',
                    ],
                    [
                        'name'   => '上传',
                        'route'  => $prefix.'/attachment/upload',
                        'method' => 'POST',
                        'filter' => '@',
                    ],
                    [
                        'name'   => '删除',
                        'route'  => $prefix.'/attachment/delete',
                        'method' => 'POST',
                    ],
                ],
            ],
            [
                'name'     => 'CURD <img style="margin-left:5px;width:33px;" src="https://img.shields.io/badge/'.$curdVersion.'-green.svg">',
                'icon'     => 'ri-settings-line',
                'menu'     => true,
                'route'    => $curdPrefix,
                'children' => [
                    //                    [
                    //                        'name'   => '/',
                    //                        'menu'   => false,
                    //                        'filter' => '*',
                    //                        'route'  => $curdPrefix,
                    //                        'method' => ['*'],
                    //                    ],
                    [
                        'name'   => '/index',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/index',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/update',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/update',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/create',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/create',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/menu',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/menu',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/relation',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/relation',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/delete',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/delete',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/change',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/change',
                        'method' => ['*'],
                    ],


                    [
                        'name'   => '/page/',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/index',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page/index',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/update',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page/update',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/create',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page/create',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/delete',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page/delete',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/change',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/page/change',
                        'method' => ['*'],
                    ],

                    [
                        'name'   => '/fields/',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'fields',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/index',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/index',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/update',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/update',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/create',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/create',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/delete',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/delete',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/change',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/change',
                        'method' => ['*'],
                    ],
                    [
                        'name'   => '/relation',
                        'menu'   => false,
                        'filter' => '*',
                        'route'  => $curdPrefix.'/fields/relation',
                        'method' => ['*'],
                    ],
                ],
            ],
        ],
    ],

    [
        'name'     => '会员中心',
        'route'    => '#centor',
        'icon'     => 'ri-user-4-line',
        'menu'     => true,
        'children' => [
            [
                'name'     => '会员管理',
                'icon'     => 'ri-user-3-line',
                'menu'     => true,
                'route'    => $curdPrefix . '/page/user',
                'children' => [
                    [
                        'name'   => '创建',
                        'method' => '*',
                        'route'  => $curdPrefix . '/page/create/user',
                    ],
                    [
                        'name'   => '更新',
                        'route'  => $curdPrefix . '/page/update/user',
                        'method' => '*',
                    ],
                    [
                        'name'   => '修改',
                        'route'  => $curdPrefix . '/page/change/user',
                        'method' => 'POST',
                    ],
                    [
                        'name'   => '删除',
                        'route'  => $curdPrefix . '/page/delete/user',
                        'method' => 'POST',
                    ],
                ],
            ],
            [
                'name'     => '文章管理',
                'icon'     => 'ri-file-list-3-line',
                'menu'     => true,
                'route'    => '#article',
                'children' => [
                    [
                        'name'     => '文章管理',
                        'icon'     => 'ri-file-list-3-line',
                        'menu'     => true,
                        'route'    => $curdPrefix . '/page/post',
                        'children' => [
                            [
                                'name'   => '创建',
                                'method' => '*',
                                'route'  => $curdPrefix . '/page/create/post',
                            ],
                            [
                                'name'   => '更新',
                                'route'  => $curdPrefix . '/page/update/post',
                                'method' => '*',
                            ],
                            [
                                'name'   => '修改',
                                'route'  => $curdPrefix . '/page/change/post',
                                'method' => 'POST',
                            ],
                            [
                                'name'   => '删除',
                                'route'  => $curdPrefix . '/page/delete/post',
                                'method' => 'POST',
                            ],
                        ],
                    ],
                    [
                        'name'     => '标签管理',
                        'icon'     => 'ri-file-list-3-line',
                        'menu'     => true,
                        'route'    => $curdPrefix . '/page/tags',
                        'children' => [
                            [
                                'name'   => '创建',
                                'method' => '*',
                                'route'  => $curdPrefix . '/page/create/tags',
                            ],
                            [
                                'name'   => '更新',
                                'route'  => $curdPrefix . '/page/update/tags',
                                'method' => '*',
                            ],
                            [
                                'name'   => '修改',
                                'route'  => $curdPrefix . '/page/change/tags',
                                'method' => 'POST',
                            ],
                            [
                                'name'   => '删除',
                                'route'  => $curdPrefix . '/page/delete/tags',
                                'method' => 'POST',
                            ],
                        ],
                    ],
                ]
            ],

        ],
    ],
];


