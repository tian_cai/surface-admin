<?php

$upload_url = '/backend/attachment/upload';
$manage_url = '/backend/attachment/manage';

return [
    'route_prefix' => 'curd',     // 路由前缀 通过访问 /curd.html 访问表格页面
    'middleware' => \app\backend\services\auth\Middleware::class,
    'save' => 'file', // 默认 存储类型
    'engine' => [
        'file' => [
            'save_path' => root_path('curd'), // 数据保存地址 确保路径的可读写
        ]
    ],

    /*
     * surface配置 配置优先级: 自定义配置 > 默认配置
     *
     * globals参数为VUE组件（props，domProps，style，class ....）
     *
     * 前端使用 ElementUI
     *
     * Form.globals.props 参数 https://element.eleme.cn/#/zh-CN/component/form#form-attributes
     * Table.globals.props 参数 https://element.eleme.cn/#/zh-CN/component/table#table-attributes
     */
    'surface' => [
        // 静态资源CDN地址 默认使用公共cdn 如果需要自定义cdn地址 请下载静态资源 https://gitee.com/iszsw/surface-src
//        'cdn' => '/static/surface',
        'table' => [
            'globals' => [
                'props' => [ // props参数
                    'emptyText' => "没有更多",
                ],
            ],
        ],
        'form'  => [
            'globals' => [

            ],
            'upload' => [
                'props' => [
                    'action'     => $upload_url,    // 文件上传地址
                    'manageUrl'  => $manage_url,    // 文件管理地址
                    'uploadType' => 'image', // 文件类型 支持image|file
                    'multiple'   => false,
                    'limit'      => 1,
                ],
            ],
            'editor' => [
                'props' => [
                    'uploadUrl' => $upload_url, // 上传地址
                    'manageUrl' => $manage_url, // 文件管理地址
                    'typeName'  => 'type',        // 不同类型文件上传携带的参数 类型KEY
                    'config'    => [                // wangEditor配置参数
                                                    'showLinkImgAlt' => false,
                    ],
                ],
            ],
        ],
    ]
];
